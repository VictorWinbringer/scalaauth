/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.auth_scala.app.controllers

import cats.effect._
import eu.timepit.refined.auto._
import eu.timepit.refined.types.string.NonEmptyString
import fs2.Stream
import org.http4s._
import org.http4s.circe._
import sttp.model.{Header, HeaderNames, QueryParams, StatusCode}
import sttp.tapir.generic.auto._
import sttp.tapir.json.circe._
import vw.auth_scala.logic.domain.entities
import vw.auth_scala.logic.domain.entities._
import vw.auth_scala.app.models._
import sttp.tapir._
import sttp.tapir.server._
import io.circe.generic.auto._
import io.circe.generic.semiauto._
import sttp.tapir.codec.refined._

import java.io.File
import java.io.{File, InputStream}
import java.nio.ByteBuffer
import java.nio.charset.{Charset, StandardCharsets}
import java.nio.file.{Path, Paths}
import sttp.tapir.CodecFormat.{Json, OctetStream, TextPlain, Xml}
import sttp.tapir.model.ServerRequest
import sttp.tapir.typelevel.MatchType
import sttp.capabilities.fs2.Fs2Streams
import cats.implicits._
import vw.auth_scala.logic.primaryAdapters.{AuthService, HelloService}
import vw.auth_scala.logic.secondaryAdapters.MyAppCompiler

final class HelloWorldController(
                                  implicit helloService: HelloService,
                                  authService: AuthService,
                                  myAppCompiler: MyAppCompiler
                                ) {

  implicit def decodeGreetings: EntityDecoder[IO, Greetings] = jsonOf

  implicit def encodeGreetings: EntityEncoder[IO, Greetings] = jsonEncoderOf

  private val example = entities.Greetings(
    title = "Hello Kirk!",
    headings = "Hello Kirk, live long and prosper!",
    message = "This is some demo message..."
  )

  private def hello(name: NonEmptyString): IO[Either[vw.auth_scala.app.models.Error, Greetings]] =
    BaseController.compile[Greetings](helloService.sayHello(name), StatusCode.BadRequest)

  private def auth(user: User, name: NonEmptyString): IO[Either[vw.auth_scala.app.models.Error, String]] =
    BaseController.compile[String](helloService.checkAuth(user, name), StatusCode.BadRequest)

  private def uploadFile(stream: fs2.Stream[IO, Byte]) =
    helloService.upload(stream).foldMap(myAppCompiler).map(_.asRight[vw.auth_scala.app.models.Error])

  private def downloadFile(id: NonEmptyString): IO[Either[vw.auth_scala.app.models.Error, Stream[IO, Byte]]] = {
    val path = Paths.get(s"images/${id}")
    helloService.download(path).foldMap(myAppCompiler).map(x => x.asRight)
  }

  private val sayHello = BaseController.baseEndpoint
    .get
    .in("hello")
    .tag("Hello")
    .in("greetings")
    .in(query[NonEmptyString]("name"))
    .out(jsonBody[Greetings].description("A JSON object demo").example(example))
    .description(
      "Returns a simple JSON object using the provided query parameter 'name' which must not be empty."
    )
    .serverLogic(hello)

  private val checkAuth = BaseController.baseEndpointWithAuth
    .get
    .in("hello")
    .tag("Hello")
    .in("auth")
    .in(query[NonEmptyString]("name"))
    .out(stringBody)
    .serverLogic({ case (user, name) => auth(user, name) })

  private val upload = BaseController.baseEndpoint
    .summary("Загрузить файл картинки на сервер")
    .description("Загружает выбранную картинку")
    .in("hello")
    .tag("Hello")
    .in("upload")
    .in(streamBinaryBody(Fs2Streams[IO]))
    .post
    .serverLogic(uploadFile)

  private val download = BaseController.baseEndpoint
    .summary("Скачать картинку")
    .description("Скачивает картику по ее идентификатору")
    .in("hello")
    .tag("Hello")
    .in("download")
    .get
    .in(query[NonEmptyString]("name"))
    .out(streamBinaryBody(Fs2Streams[IO]))
    .serverLogic(downloadFile)

  val endpoints = List(sayHello, checkAuth, upload, download)
}
