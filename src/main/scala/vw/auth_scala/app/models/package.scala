/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.auth_scala.app

import eu.timepit.refined.api._
import eu.timepit.refined.cats._
import eu.timepit.refined.collection._

package object models {

}
