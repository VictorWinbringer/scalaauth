/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.auth_scala.logic.primaryAdapters

import cats.effect.IO
import cats.free.Free
import cats.implicits.{catsSyntaxEitherId, _}
import eu.timepit.refined.auto._
import eu.timepit.refined.cats._
import eu.timepit.refined.types.string.NonEmptyString
import vw.auth_scala.logic.domain.entities
import vw.auth_scala.logic.domain.entities.{Greetings, User}
import vw.auth_scala.logic.domain.services.{FilesRepository, MyApp, UuidRepository}

import java.nio.file.{Path, Paths}

final class HelloService(implicit uuidRepository: UuidRepository, filesRepository: FilesRepository) {
  final val message: NonEmptyString = "This is a fancy message directly from http4s! :-)"

  def sayHello(name: NonEmptyString): Free[MyApp, Either[String, Greetings]] = {
    val greetings = (
      NonEmptyString.from(s"Hello ${name.show}!").toOption,
      NonEmptyString.from(s"Hello ${name.show}, live long and prosper!").toOption
      ).mapN { case (title, headings) =>
      entities.Greetings(
        title = title,
        headings = headings,
        message = message
      )
    }
    Free.pure(greetings.fold("Not Found".asLeft[Greetings])(_.asRight[String]))
  }

  def checkAuth(user: User, name: NonEmptyString): Free[MyApp, Either[String, String]] =
    Free.pure(s"${user} ${name}".asRight[String])

  def upload(stream: fs2.Stream[IO, Byte]): Free[MyApp, Unit] = {
    for {
      id <- uuidRepository.create()
      path = Paths.get(s"images/${id}")
      res <- filesRepository.upload(stream, path)
    } yield res
  }

  def download(path: Path): Free[MyApp, fs2.Stream[IO, Byte]] = filesRepository.download(path)
}
