/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.auth_scala.logic.primaryAdapters

import cats.data.EitherT
import cats.free.Free
import eu.timepit.refined.types.string.NonEmptyString
import vw.auth_scala.logic.domain.entities.{AuthToken, User}
import vw.auth_scala.logic.domain.services.{MyApp, TokenParser, UserProfileRepository, UuidRepository}

final class AuthService(implicit profiles: UserProfileRepository, uuidRepository: UuidRepository, tokenParser: TokenParser) {
  def auth(token: AuthToken): Free[MyApp, Either[String, User]] = {
    val res = for {
      userId <- EitherT(tokenParser.parse(token))
      id <- EitherT.right(uuidRepository.create())
      profile <- EitherT.fromOptionF(profiles.get(NonEmptyString.unsafeFrom(userId.toString())), "User Not Found")
    } yield User(userId, profile, id.toString)
   val r = res.value
    r
  }
}


