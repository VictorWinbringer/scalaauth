/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.auth_scala.logic.domain

import eu.timepit.refined.api.RefType.refinedRefType
import eu.timepit.refined.api._
import eu.timepit.refined.cats.CatsRefinedTypeOpsSyntax
import eu.timepit.refined.collection._
import eu.timepit.refined.numeric._
import eu.timepit.refined.types.all.NonEmptyFiniteString

package object entities {
  type UserAge = Int Refined Interval.ClosedOpen[18, 150]

  object UserAge extends RefinedTypeOps[UserAge, Int] with CatsRefinedTypeOpsSyntax

  type UserId = String Refined Size[Interval.Closed[1, 255]]

  object UserId extends RefinedTypeOps[UserId, String] with CatsRefinedTypeOpsSyntax

  type UserRoles = List[NonEmptyFiniteString[255]]
  type AuthToken = String Refined NonEmpty

  object AuthToken extends RefinedTypeOps[AuthToken, String] with CatsRefinedTypeOpsSyntax

  type GreetingTitle = String Refined NonEmpty

  object GreetingTitle extends RefinedTypeOps[GreetingTitle, String] with CatsRefinedTypeOpsSyntax

  type GreetingHeader = String Refined NonEmpty

  object GreetingHeader extends RefinedTypeOps[GreetingHeader, String] with CatsRefinedTypeOpsSyntax

  type GreetingMessage = String Refined NonEmpty

  object GreetingMessage extends RefinedTypeOps[GreetingMessage, String] with CatsRefinedTypeOpsSyntax

}
