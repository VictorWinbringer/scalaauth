/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.auth_scala.logic.domain.services

import cats.free.Free

import java.util.UUID

sealed trait UuidRepositoryOps[A] extends MyApp[A]

case class Create() extends UuidRepositoryOps[UUID]

final class UuidRepository {
  def create(): Free[MyApp, UUID] = Free.liftF(Create())
}