/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.auth_scala.logic.domain.services

import cats.effect.IO
import cats.free.{Free, FreeT}

import java.nio.file.Path
import scala.io._

sealed trait FilesRepositoryOps[A] extends MyApp[A]

case class Upload(stream: fs2.Stream[IO, Byte], path: Path) extends FilesRepositoryOps[Unit]

case class Download(path: Path) extends FilesRepositoryOps[fs2.Stream[IO, Byte]]

final class FilesRepository {

  type FilesRepositoryT[M[_], A] = FreeT[FilesRepositoryOps, M, A]

  def upload(stream: fs2.Stream[IO, Byte], path: Path): Free[MyApp, Unit] = Free.liftF(Upload(stream, path))

  def download(path: Path): Free[MyApp, fs2.Stream[IO, Byte]] = Free.liftF(Download(path))


  import cats.free._
  import cats._
  import cats.data._

  /* A base ADT for the user interaction without state semantics */
  sealed abstract class Teletype[A] extends Product with Serializable

  final case class WriteLine(line: String) extends Teletype[Unit]

  final case class ReadLine(prompt: String) extends Teletype[String]

  type TeletypeT[M[_], A] = FreeT[Teletype, M, A]
  type Log = List[String]

  type TeletypeState[A] = State[List[String], A]

  /** Teletype smart constructors */
  object TeletypeOps {
    def writeLine(line: String): TeletypeT[TeletypeState, Unit] =
      FreeT.liftF[Teletype, TeletypeState, Unit](WriteLine(line))

    def readLine(prompt: String): TeletypeT[TeletypeState, String] =
      FreeT.liftF[Teletype, TeletypeState, String](ReadLine(prompt))

    def log(s: String): TeletypeT[TeletypeState, Unit] =
      FreeT.liftT[Teletype, TeletypeState, Unit](State.modify(s :: _))
  }

  def program: TeletypeT[TeletypeState, Unit] = {
    for {
      userSaid <- TeletypeOps.readLine("what's up?!")
      _ <- TeletypeOps.log(s"user said : $userSaid")
      _ <- TeletypeOps.writeLine("thanks, see you soon!")
    } yield ()
  }

  import cats.data.EitherK
  import cats.free.Free
  import cats.{Id, InjectK, ~>}
  import scala.collection.mutable.ListBuffer

  sealed trait Interact[A]

  case class Ask(prompt: String) extends Interact[String]

  case class Tell(msg: String) extends Interact[Unit]

  /* Represents persistence operations */
  sealed trait DataOp[A]

  case class AddCat(a: String) extends DataOp[Unit]

  case class GetAllCats() extends DataOp[List[String]]

  type CatsApp[A] = EitherK[DataOp, Interact, A]

  class Interacts[F[_]](implicit I: InjectK[Interact, F]) {
    def tell(msg: String): Free[F, Unit] = Free.liftInject[F](Tell(msg))

    def ask(prompt: String): Free[F, String] = Free.liftInject[F](Ask(prompt))
  }

  object Interacts {
    implicit def interacts[F[_]](implicit I: InjectK[Interact, F]): Interacts[F] = new Interacts[F]
  }

  class DataSource[F[_]](implicit I: InjectK[DataOp, F]) {
    def addCat(a: String): Free[F, Unit] = Free.liftInject[F](AddCat(a))

    def getAllCats: Free[F, List[String]] = Free.liftInject[F](GetAllCats())
  }

  object DataSource {
    implicit def dataSource[F[_]](implicit I: InjectK[DataOp, F]): DataSource[F] = new DataSource[F]
  }

  def programSum(implicit I: Interacts[CatsApp], D: DataSource[CatsApp]): Free[CatsApp, Unit] = {

    import I._, D._

    for {
      cat <- ask("What's the kitty's name?")
      _ <- addCat(cat)
      cats <- getAllCats
      _ <- tell(cats.toString)
    } yield ()
  }

  object ConsoleCatsInterpreter extends (Interact ~> Id) {
    def apply[A](i: Interact[A]) = i match {
      case Ask(prompt) =>
        println(prompt)
        Id("ddd")
      case Tell(msg) =>
        Id(println(msg))
    }
  }

  object InMemoryDatasourceInterpreter extends (DataOp ~> Id) {

    private[this] val memDataSet = new ListBuffer[String]

    def apply[A](fa: DataOp[A]) = fa match {
      case AddCat(a) => memDataSet.append(a); Id(())
      case GetAllCats() => Id(memDataSet.toList)
    }
  }

  val interpreter: CatsApp ~> Id = InMemoryDatasourceInterpreter or ConsoleCatsInterpreter
}
