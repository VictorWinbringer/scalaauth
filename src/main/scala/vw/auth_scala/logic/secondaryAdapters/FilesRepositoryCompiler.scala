/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.auth_scala.logic.secondaryAdapters

import cats.effect.{Blocker, ContextShift, IO, Sync}
import cats.{Applicative, ~>}
import fs2.io
import vw.auth_scala.logic.domain.services.{Download, FilesRepositoryOps, MyApp, Upload}

import java.nio.file.StandardOpenOption

final class FilesRepositoryCompiler(val blocker: Blocker)(implicit contextShift: ContextShift[IO]) extends (FilesRepositoryOps ~> IO) {
  override def apply[A](fa: FilesRepositoryOps[A]): IO[A] =
    fa match {
      case up: Upload => Applicative[IO].map(up.stream
        .through(fs2.io.file.writeAll[IO](up.path, blocker, List(StandardOpenOption.CREATE, StandardOpenOption.WRITE)))
        .compile
        .drain)(_.asInstanceOf[A])
      case vd: Download => Applicative[IO].pure(io.file.readAll[IO](vd.path, blocker, 4096))
    }
}
