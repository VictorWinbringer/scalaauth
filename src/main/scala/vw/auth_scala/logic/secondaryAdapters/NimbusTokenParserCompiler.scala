/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.auth_scala.logic.secondaryAdapters

import cats.{Applicative, ~>}
import com.nimbusds.jose._
import com.nimbusds.jose.util.DefaultResourceRetriever
import com.nimbusds.jwt._
import com.nimbusds.oauth2.sdk.id._
import com.nimbusds.openid.connect.sdk.validators._
import vw.auth_scala.app.config.AuthOConfig
import vw.auth_scala.logic.domain.entities.UserId
import vw.auth_scala.logic.domain.services.{Parse, TokenParserOps}

import java.net._
import scala.util._

final class NimbusTokenParserCompiler[F[_] : Applicative](implicit _config: AuthOConfig) extends (TokenParserOps ~> F) {
  override def apply[A](fa: TokenParserOps[A]): F[A] = fa match {
    case Parse(token) => {
      val res = Try[String]({
        val iss = new Issuer(_config.domain.value + "/")
        val clientID = new ClientID(_config.clientId.value)
        val jwsAlg = JWSAlgorithm.RS256
        val jwkSetURL = new URL(_config.domain.value + "/.well-known/jwks.json")
        val validator = new IDTokenValidator(iss, clientID, jwsAlg, jwkSetURL, new DefaultResourceRetriever())
        val idToken = JWTParser.parse(token.value)
        val claims = validator.validate(idToken, null)
        claims.getSubject().getValue
      }).toEither
        .swap
        .map(x => x.getMessage)
        .swap
        .flatMap(x => UserId.from(x))
      Applicative[F].pure(res)
    }
  }
}
//eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjNuUmF0OEgzODBCN1VfZG9JXy05SCJ9.eyJpc3MiOiJodHRwczovL2Rldi10LWNhM2s5Mi51cy5hdXRoMC5jb20vIiwic3ViIjoiekdNYjB1NTg0dkdOUVN3eTdLQ0U0UXlBbHZMc1NVWDdAY2xpZW50cyIsImF1ZCI6Imh0dHA6Ly8xMjcuMC4wLjE6ODg4OCIsImlhdCI6MTY0NTQ3MTMxNywiZXhwIjoxNjQ1NTU3NzE3LCJhenAiOiJ6R01iMHU1ODR2R05RU3d5N0tDRTRReUFsdkxzU1VYNyIsImd0eSI6ImNsaWVudC1jcmVkZW50aWFscyJ9.pIRYdTUKqjyh--RoYyn4LzsCDC5rmRfuXkcjayUR43eny_DDxo850QDZOSk3gJ3jVF3JTy9HVAvDd7NUuxjBpWavOSQ4qHLvnUXTjHc1NPPPlIBQ_B9Srvvg7ER6D4j0X6xOo2kM9JbBulW1jfsHZyChtVoIPRTxMtwFEXrV85K25r9ner6a6UkjSwNQ9eiEp_WtFD8IwRYCkJjA-C4yq96074A4HQSCEvEFtJGy4e2ePkykb4MHtTufE2b6-UxjW_oQ3sJRw2nDe9UU8NsVSgcFnQj1j5MuK-08w5lhPNucWNgymDQLkHabXKBqw5A2RgMQ_wLYf4CF3W423rdTvw
