/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.auth_scala.logic.secondaryAdapters

import cats.effect.ExitCode.Error
import cats.effect.IO
import cats.~>
import vw.auth_scala.logic.domain.services.{FilesRepositoryOps, MyApp, TokenParserOps, UserProfileRepositoryOps, UuidRepositoryOps}

final class MyAppCompiler(
                           implicit userRepositoryCompiler: UserProfileRepositoryCompiler[IO],
                           uuidRepositoryCompiler: UuidRepositoryCompiler[IO],
                           tokenParserCompiler: NimbusTokenParserCompiler[IO],
                           filesRepositoryCompiler: FilesRepositoryCompiler
                         ) extends (MyApp ~> IO) {
  override def apply[A](fa: MyApp[A]): IO[A] = fa match {
    case upro: UserProfileRepositoryOps[A] => userRepositoryCompiler.apply(upro)
    case uro: UuidRepositoryOps[A] => uuidRepositoryCompiler.apply(uro)
    case tpo: TokenParserOps[A] => tokenParserCompiler.apply(tpo)
    case fro: FilesRepositoryOps[A] => filesRepositoryCompiler.apply(fro)
    case x => throw new Exception(x.toString)
  }
}
